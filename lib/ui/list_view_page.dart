import 'package:flutter/material.dart';

class ListViewPage extends StatelessWidget {
  const ListViewPage();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("List"),
      ),
      body: SafeArea(
        child: Container(
          color: Colors.white,
          child: ListView.builder(
            itemBuilder: (context, position) {
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: ElevatedButton(
                  onPressed: () {},
                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.blueGrey)),
                  child: Text("Item Number ${position + 1}"),
                ),
              );
            },
            itemCount: 100,
            padding: const EdgeInsets.all(16),
          ),
        ),
      ),
    );
  }
}
