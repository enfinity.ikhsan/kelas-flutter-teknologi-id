import 'package:flutter/material.dart';

class ButtonPage extends StatelessWidget {
  const ButtonPage();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Button"),
      ),
      body: SafeArea(
        child: Container(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                TextButton(
                  onPressed: () {},
                  style: ButtonStyle(
                    overlayColor: MaterialStateProperty.all(Colors.green),
                  ),
                  child: Text("Text Button"),
                ),
                ElevatedButton(
                  onPressed: () {},
                  style: ButtonStyle(
                    elevation: MaterialStateProperty.all(10),
                  ),
                  child: Text("Elevated Button"),
                ),
                OutlinedButton(
                  onPressed: () {},
                  style: ButtonStyle(
                    side: MaterialStateProperty.all(BorderSide(color: Colors.green, width: 1)),
                  ),
                  child: Text("Outlined Button"),
                ),
                IconButton(
                  color: Colors.red,
                  onPressed: () {},
                  icon: Icon(Icons.info_outline),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
