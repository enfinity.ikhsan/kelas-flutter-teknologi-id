import 'package:flutter/material.dart';

class ContainerPage extends StatelessWidget {
  const ContainerPage();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Container"),
      ),
      body: SafeArea(
        child: Container(
          width: double.infinity,
          height: double.infinity,
          color: Colors.grey,
          child: Container(
            padding: const EdgeInsets.all(30),
            margin: const EdgeInsets.all(60),
            color: Colors.green,
            child: Container(
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
}
