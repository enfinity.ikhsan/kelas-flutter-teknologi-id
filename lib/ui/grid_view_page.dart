import 'package:flutter/material.dart';

class GridViewPage extends StatelessWidget {
  const GridViewPage();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("GridView"),
      ),
      body: SafeArea(
        child: Container(
          color: Colors.white,
          child: GridView.builder(
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                crossAxisSpacing: 8,
                mainAxisSpacing: 8,
              ),
              itemBuilder: (context, position) {
                return ElevatedButton(
                  onPressed: () {},
                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.blueGrey)),
                  child: Text("Item Number ${position + 1}"),
                );
              },
            itemCount: 100,
          ),
        ),
      ),
    );
  }
}