import 'package:flutter/material.dart';

class ScaffoldPage extends StatelessWidget {
  const ScaffoldPage();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("This is my AppBar"),
      ),
      body: Center(
        child: Text("This my Body"),
      ),
      bottomNavigationBar: Container(
        height: kToolbarHeight,
        color: Colors.blueGrey,
        child: Center(
          child: Text("This is my BottomNavigationBar"),
        ),
      ),
    );
  }
}
