import 'package:flutter/material.dart';

class DropdownPage extends StatefulWidget {
  const DropdownPage();

  @override
  _DropdownPageState createState() => _DropdownPageState();
}

class _DropdownPageState extends State<DropdownPage> {
  List<String> cities = ["Jakarta", "Bandung", "Semarang", "Surabaya", "Palangkaraya", "Makassar", "Riau"];
  String selection = "Jakarta";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Dropdown"),
      ),
      body: SafeArea(
        child: Container(
          color: Colors.white,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                DropdownButton<String>(
                  value: selection,
                  items: cities
                      .map((e) => DropdownMenuItem<String>(
                            value: e,
                            child: Text(e),
                          ))
                      .toList(),
                  onChanged: (value) {
                    setState(() {
                      selection = value;
                    });
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
