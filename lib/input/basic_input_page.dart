import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';

class BasicInputPage extends StatefulWidget {
  @override
  _BasicInputPageState createState() => _BasicInputPageState();
}

class _BasicInputPageState extends State<BasicInputPage> {
  final TextEditingController textEditingController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController numberController = TextEditingController();
  final TextEditingController multilineController = TextEditingController();

  bool isPassword = true;
  
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    textEditingController.dispose();
    passwordController.dispose();
    numberController.dispose();
    multilineController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Basic Input"),
      ),
      body: Container(
        padding: const EdgeInsets.all(16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Flexible(
                  child: TextField(
                    controller: textEditingController,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(hintText: "Ketik nama Anda"),
                    textCapitalization: TextCapitalization.words,
                  ),
                ),
                ElevatedButton(
                  onPressed: () {
                    ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(content: Text("Nama saya ${textEditingController.text}")),
                    );
                  },
                  child: Text("OK"),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Flexible(
                  child: TextField(
                    controller: passwordController,
                    keyboardType: TextInputType.text,
                    obscureText: isPassword,
                    decoration: InputDecoration(
                      hintText: "Ketik password",
                      suffixIcon: IconButton(
                        onPressed: () {
                          setState(() {
                            isPassword = !isPassword;
                          });
                        },
                        icon: Icon(isPassword ? Icons.visibility : Icons.visibility_off),
                      )
                    ),
                  ),
                ),
                ElevatedButton(
                  onPressed: () {
                    ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(content: Text("Password kamu ${passwordController.text}")),
                    );
                  },
                  child: Text("OK"),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Flexible(
                  child: TextField(
                    controller: numberController,
                    keyboardType: TextInputType.number,
                    maxLength: 15,
                    decoration: InputDecoration(
                      hintText: "Ketik nomor telepon kamu",
                    ),
                  ),
                ),
                ElevatedButton(
                  onPressed: () {
                    ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(content: Text("Nomor telepon kamu ${numberController.text}")),
                    );
                  },
                  child: Text("OK"),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Flexible(
                  child: TextField(
                    controller: multilineController,
                    keyboardType: TextInputType.multiline,
                    minLines: 3,
                    maxLines: 10,
                    decoration: InputDecoration(
                      hintText: "Ketik teks yang sangat panjang",
                    ),
                  ),
                ),
                ElevatedButton(
                  onPressed: () {
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(content: Text("Teks yang telah kamu ketik kamu ${multilineController.text}")),
                    );
                  },
                  child: Text("OK"),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
