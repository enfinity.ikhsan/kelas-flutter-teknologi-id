import 'package:flutter/material.dart';

class SliderPage extends StatefulWidget {
  const SliderPage();

  @override
  _SliderPageState createState() => _SliderPageState();
}

class _SliderPageState extends State<SliderPage> {
  double slider = 0;
  double rangeStart = 0;
  double rangeEnd = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Slider"),
      ),
      body: SafeArea(
        child: Container(
          color: Colors.white,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  children: [
                    Text("Slider value : $slider"),
                    Slider(
                      value: slider,
                      divisions: 20,
                      onChanged: (value) {
                        setState(() {
                          slider = value;
                        });
                      },
                    ),
                  ],
                ),
                Column(
                  children: [
                    Text("Range Slider value start : $rangeStart ; end : $rangeEnd"),
                    RangeSlider(
                      values: RangeValues(rangeStart, rangeEnd),
                      divisions: 50,
                      onChanged: (value) {
                        setState(() {
                          rangeStart = value.start;
                          rangeEnd = value.end;
                        });
                      },
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
