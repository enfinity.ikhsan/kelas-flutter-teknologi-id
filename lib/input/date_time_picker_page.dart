import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DateTimePickerPage extends StatelessWidget {
  const DateTimePickerPage();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Date Time Picker"),
      ),
      body: SafeArea(
        child: Container(
          color: Colors.white,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                ElevatedButton(
                  onPressed: () async {
                    final date = await showDatePicker(
                      context: context,
                      initialDate: DateTime.now(),
                      firstDate: DateTime(2010),
                      lastDate: DateTime(2022),
                    );

                    final dateFormat = DateFormat("dd MMMM y", "id_ID");

                    ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(content: Text(dateFormat.format(date))),
                    );
                  },
                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.blueGrey)),
                  child: Text("Show Date Picker"),
                ),
                ElevatedButton(
                  onPressed: () async {
                    final time = await showTimePicker(context: context, initialTime: TimeOfDay.now());

                    ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(content: Text(time.toString())),
                    );
                  },
                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.blueGrey)),
                  child: Text("Show Time Picker"),
                ),
                ElevatedButton(
                  onPressed: () async {
                    final time = await showDateRangePicker(
                      context: context,
                      initialDateRange: DateTimeRange(
                        start: DateTime.now(),
                        end: DateTime.now().add(Duration(days: 2)),
                      ),
                      firstDate: DateTime(2010),
                      lastDate: DateTime(2022),
                    );
                    
                    ScaffoldMessenger.of(context).hideCurrentSnackBar();
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(content: Text(time.toString())),
                    );
                  },
                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.blueGrey)),
                  child: Text("Show Date Range Picker"),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
