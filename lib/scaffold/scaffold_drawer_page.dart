import 'package:flutter/material.dart';

class ScaffoldDrawerPage extends StatefulWidget {
  @override
  _ScaffoldDrawerPageState createState() => _ScaffoldDrawerPageState();
}

class _ScaffoldDrawerPageState extends State<ScaffoldDrawerPage> {
  Color _contentColor = Colors.white;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Drawer"),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              decoration: BoxDecoration(color: Colors.red),
              child: Text("This is drawer header"),
            ),
            ListTile(
              title: Text("Drawer menu 1"),
              onTap: () {
                setState(() {
                  _contentColor = Colors.green;
                  Navigator.pop(context);
                });
              },
            ),
            ListTile(
              title: Text("Drawer menu 2"),
              onTap: () {
                setState(() {
                  _contentColor = Colors.purple;
                  Navigator.pop(context);
                });
              },
            ),
            ListTile(
              title: Text("Drawer menu 3"),
              onTap: () {
                setState(() {
                  _contentColor = Colors.blueAccent;
                  Navigator.pop(context);
                });
              },
            ),
            ListTile(
              title: Text("Drawer menu 4"),
              onTap: () {
                setState(() {
                  _contentColor = Colors.yellow;
                  Navigator.pop(context);
                });
              },
            ),
          ],
        ),
      ),
      endDrawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              decoration: BoxDecoration(color: Colors.purpleAccent),
              child: Text("This is end drawer header"),
            ),
            ListTile(
              title: Text("Drawer menu 1"),
              onTap: () {
                setState(() {
                  _contentColor = Colors.green;
                  Navigator.pop(context);
                });
              },
            ),
            ListTile(
              title: Text("Drawer menu 2"),
              onTap: () {
                setState(() {
                  _contentColor = Colors.purple;
                  Navigator.pop(context);
                });
              },
            ),
            ListTile(
              title: Text("Drawer menu 3"),
              onTap: () {
                setState(() {
                  _contentColor = Colors.blueAccent;
                  Navigator.pop(context);
                });
              },
            ),
            ListTile(
              title: Text("Drawer menu 4"),
              onTap: () {
                setState(() {
                  _contentColor = Colors.yellow;
                  Navigator.pop(context);
                });
              },
            ),
          ],
        ),
      ),
      body: SafeArea(
        child: Container(
          color: _contentColor,
          child: Center(
            child: Text("Click Top Left or Top Right Menu to Open Drawer"),
          ),
        ),
      ),
    );
  }
}
