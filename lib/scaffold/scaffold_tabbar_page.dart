import 'package:flutter/material.dart';

class ScaffoldTabBarPage extends StatelessWidget {
  const ScaffoldTabBarPage();

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Tab Bar View"),
          bottom: TabBar(tabs: [
            Tab(icon: Icon(Icons.map),),
            Tab(icon: Icon(Icons.email),),
            Tab(icon: Icon(Icons.account_circle),)
          ],),
        ),
        body: SafeArea(
          child: Container(
            color: Colors.white,
            child: TabBarView(children: [
              Container(color: Colors.red,),
              Container(color: Colors.blue,),
              Container(color: Colors.purple,),
            ]),
          ),
        ),
      ),
    );
  }
}
