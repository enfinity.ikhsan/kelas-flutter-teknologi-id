import 'package:flutter/material.dart';

class ScaffoldActionButtonPage extends StatefulWidget {
  @override
  _ScaffoldActionButtonPageState createState() => _ScaffoldActionButtonPageState();
}

class _ScaffoldActionButtonPageState extends State<ScaffoldActionButtonPage> {
  int _selectedMenuIndex = 0;
  Color _selectedColor = Colors.white;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Action Button"),
        actions: [
          IconButton(
            onPressed: () {
              setState(() {
                _selectedColor = Colors.red;
              });
            },
            icon: Icon(Icons.add),
          ),
          IconButton(
            onPressed: () {
              setState(() {
                _selectedColor = Colors.blue;
              });
            },
            icon: Icon(Icons.edit),
          ),
          PopupMenuButton<int>(
            itemBuilder: (context) {
              return [
                PopupMenuItem(
                  value: 0,
                  child: Row(
                    children: [
                      Icon(
                        Icons.settings,
                        color: Colors.grey,
                      ),
                      SizedBox(
                        width: 16,
                      ),
                      Text("Settings"),
                    ],
                  ),
                ),
                PopupMenuItem(
                  value: 1,
                  child: Row(
                    children: [
                      Icon(
                        Icons.info_outline,
                        color: Colors.grey,
                      ),
                      SizedBox(
                        width: 16,
                      ),
                      Text("About"),
                    ],
                  ),
                ),
                PopupMenuItem(
                  value: 2,
                  child: Row(
                    children: [
                      Icon(
                        Icons.logout,
                        color: Colors.grey,
                      ),
                      SizedBox(
                        width: 16,
                      ),
                      Text("Log Out"),
                    ],
                  ),
                ),
              ];
            },
            onSelected: (value) {
              setState(() {
                switch (value) {
                  case 0:
                    _selectedColor = Colors.purple;
                    break;
                  case 1:
                    _selectedColor = Colors.brown;
                    break;
                  case 2:
                    _selectedColor = Colors.orange;
                }
              });
            },
          ),
        ],
      ),
      body: SafeArea(
        child: Container(
          color: _selectedColor,
          child: Center(
            child: Text(
              "Click On Action Button\nTo Change Background Color",
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ),
    );
  }
}
