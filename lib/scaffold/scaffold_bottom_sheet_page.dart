import 'package:flutter/material.dart';

class ScaffoldBottomSheetPage extends StatelessWidget {
  const ScaffoldBottomSheetPage();

  final text =
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Bottom Sheet"),
      ),
      body: Builder(builder: (context) {
        return SafeArea(
          child: Container(
            color: Colors.white,
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ElevatedButton(
                    onPressed: () {
                      showBottomSheet(
                        backgroundColor: Colors.grey.shade600,
                        context: context,
                        elevation: 15,
                        builder: (context) {
                          return Container(
                            padding: const EdgeInsets.all(16),
                            margin: const EdgeInsets.only(bottom: 16),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Text(text),
                                SizedBox(
                                  height: 16,
                                ),
                                ElevatedButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: Text("Close"),
                                ),
                              ],
                            ),
                          );
                        },
                      );
                    },
                    style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.blueGrey)),
                    child: Text("Show Bottom Sheet"),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      showModalBottomSheet(
                        context: context,
                        elevation: 15,
                        builder: (context) {
                          return Container(
                            padding: const EdgeInsets.all(16),
                            margin: const EdgeInsets.only(bottom: 16),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Text(text),
                                SizedBox(
                                  height: 16,
                                ),
                                ElevatedButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                  child: Text("Close"),
                                ),
                              ],
                            ),
                          );
                        },
                      );
                    },
                    style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.blueGrey)),
                    child: Text("Show Modal Bottom Sheet"),
                  ),
                ],
              ),
            ),
          ),
        );
      }),
      bottomSheet: BottomSheet(
        backgroundColor: Colors.white,
        elevation: 15,
        enableDrag: true,
        builder: (context) {
          return DraggableScrollableSheet(
              expand: false,
              initialChildSize: 0.2,
              minChildSize: 0.2,
              maxChildSize: 0.7,
              builder: (context, controller) {
                return Container(
                  color: Colors.red,
                  child: ListView(
                    controller: controller,
                    children: [
                      Container(
                        padding: const EdgeInsets.all(16),
                        margin: const EdgeInsets.only(bottom: 16),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Text(
                              text,
                              style: TextStyle(color: Colors.white),
                            ),
                            SizedBox(
                              height: 16,
                            ),
                            Text(
                              text,
                              style: TextStyle(color: Colors.white),
                            ),
                            SizedBox(
                              height: 16,
                            ),
                            Text(
                              text,
                              style: TextStyle(color: Colors.white),
                            ),
                            SizedBox(
                              height: 16,
                            ),
                            Text(
                              text,
                              style: TextStyle(color: Colors.white),
                            ),
                            SizedBox(
                              height: 16,
                            ),
                            ElevatedButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: Text("Close"),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                );
              });
        },
        onClosing: () {},
      ),
    );
  }
}
