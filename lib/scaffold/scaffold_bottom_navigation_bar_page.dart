import 'package:flutter/material.dart';

class ScaffoldBottomNavigationBarPage extends StatefulWidget {
  @override
  _ScaffoldBottomNavigationBarPageState createState() => _ScaffoldBottomNavigationBarPageState();
}

class _ScaffoldBottomNavigationBarPageState extends State<ScaffoldBottomNavigationBarPage> {
  int _selectedMenuIndex = 0;
  Color _selectedColor = Colors.white;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Bottom Navigation Bar"),
      ),
      body: SafeArea(
        child: Container(
          color: _selectedColor,
          child: Center(
            child: Text(
              "Click On Bottom Menu\nTo Change Background Color",
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedMenuIndex,
        iconSize: 32,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.timeline),
            label: "Timeline",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.email_rounded),
            label: "Message",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle),
            label: "Profile",
          ),
        ],
        onTap: (index) {
          setState(() {
            _selectedMenuIndex = index;
            switch (index) {
              case 0:
                _selectedColor = Colors.red;
                break;
              case 1:
                _selectedColor = Colors.green;
                break;
              case 2:
                _selectedColor = Colors.blueGrey;
            }
          });
        },
      ),
    );
  }
}
