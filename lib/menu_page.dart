import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:kelas_teknologi_id/input/basic_input_page.dart';
import 'package:kelas_teknologi_id/input/date_time_picker_page.dart';
import 'package:kelas_teknologi_id/input/dropdown_page.dart';
import 'package:kelas_teknologi_id/input/form_page.dart';
import 'package:kelas_teknologi_id/input/selection_page.dart';
import 'package:kelas_teknologi_id/input/slider_page.dart';
import 'package:kelas_teknologi_id/scaffold/scaffold_action_button_page.dart';
import 'package:kelas_teknologi_id/scaffold/scaffold_bottom_navigation_bar_page.dart';
import 'package:kelas_teknologi_id/scaffold/scaffold_bottom_sheet_page.dart';
import 'package:kelas_teknologi_id/scaffold/scaffold_drawer_page.dart';
import 'package:kelas_teknologi_id/scaffold/scaffold_snackbar_dialog_page.dart';
import 'package:kelas_teknologi_id/ui/button_page.dart';
import 'package:kelas_teknologi_id/ui/column_page.dart';
import 'package:kelas_teknologi_id/ui/container_page.dart';
import 'package:kelas_teknologi_id/ui/grid_view_page.dart';
import 'package:kelas_teknologi_id/ui/image_text_page.dart';
import 'package:kelas_teknologi_id/ui/list_view_page.dart';
import 'package:kelas_teknologi_id/ui/row_page.dart';
import 'package:kelas_teknologi_id/ui/scaffold_page.dart';
import 'package:kelas_teknologi_id/ui/stack_with_align_page.dart';
import 'package:kelas_teknologi_id/scaffold/scaffold_tabbar_page.dart';

class MenuPage extends StatefulWidget {
  @override
  _MenuPageState createState() => _MenuPageState();
}

class _MenuPageState extends State<MenuPage> {
  final Map<String, Widget> listItem = {
    "Image and Text": ImageTextPage(),
    "Button": ButtonPage(),
    "Scaffold": ScaffoldPage(),
    "Container": ContainerPage(),
    "Column": ColumnPage(),
    "Rows": RowPage(),
    "ListView": ListViewPage(),
    "GridView": GridViewPage(),
    "Stack and Align": StackWithAlignPage(),
    "Tab Bar View": ScaffoldTabBarPage(),
    "Drawer": ScaffoldDrawerPage(),
    "Bottom Navigation Bar": ScaffoldBottomNavigationBarPage(),
    "Action Button": ScaffoldActionButtonPage(),
    "SnackBar and Dialog": ScaffoldSnackBarDialogPage(),
    "Bottom Sheet": ScaffoldBottomSheetPage(),
    "Basic Input": BasicInputPage(),
    "Selection": SelectionPage(),
    "Dropdown": DropdownPage(),
    "Slider": SliderPage(),
    "Date Time Picker": DateTimePickerPage(),
    "Form": FormPage(),
  };
  List<String> keys;

  @override
  void initState() {
    initializeDateFormatting("id_ID");
    keys = listItem.keys.toList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Kelas Flutter Teknologi.id",
        ),
        centerTitle: true,
      ),
      body: ListView.separated(
        itemBuilder: (context, position) {
          return ListTile(
            leading: Text("${position + 1}"),
            title: Text(keys[position]),
            trailing: Icon(Icons.chevron_right_rounded),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => listItem[keys[position]]),
              );
            },
          );
        },
        separatorBuilder: (context, position) {
          return Container(
            width: double.infinity,
            height: 1,
            color: Colors.grey,
          );
        },
        itemCount: listItem.length,
      ),
    );
  }
}
